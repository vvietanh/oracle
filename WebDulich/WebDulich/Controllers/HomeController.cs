﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebDulich.Models;
using PagedList;
using PagedList.Mvc;
using WebDulich.App_Start;

namespace WebDulich.Controllers
{
    public class HomeController : Controller
    {
        DULICHEntities1 data = new DULICHEntities1();
        
        
        public ActionResult Index(int? page)
        {
           
            int pageNumber = (page ?? 1);
            int pageSize = 9;
            // return View(data.TINTUCs.ToList());
            
            return View(data.TINTUCs.ToList().OrderBy(n => n.ID).ToPagedList(pageNumber, pageSize));
        }

        public int user = 6;

        public ActionResult Search(string sTuKhoa, int? page)
        {
            if (Request.HttpMethod != "GET")
            {
                page = 1;
            }
            int Pagesize = 9;
            int PageNumber = (page ?? 1);

            string TuKhoa = sTuKhoa;
            sTuKhoa = sTuKhoa.GenerateSlug();

            var tin = data.TINTUCs.Where(n => n.TUKHOA.Contains(sTuKhoa));
            ViewBag.TuKhoa = TuKhoa;
            return View(tin.OrderBy(n => n.TUKHOA).ToPagedList(PageNumber, Pagesize));

        }
        [HttpPost]
        public ActionResult Search(string sTuKhoa)
        {
            return RedirectToAction("Search", new { @sTuKhoa = sTuKhoa });

        }

    }
}