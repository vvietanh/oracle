﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebDulich.Models;

namespace WebDulich.Controllers
{
    public class LienheController : Controller
    {
        DULICHEntities1 data = new DULICHEntities1();
        // GET: Lienhe
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SaveMessage([Bind(Include = "HOTEN, EMAIL, SODT, NOIDUNG")] TINNHAN tn, FormCollection collection)
        {
            var noidung = collection["Noidung"];
            try
            {
                if (ModelState.IsValid)
                {
                    tn.NOIDUNG = noidung;
                    data.TINNHANs.Add(tn);
                    data.SaveChanges();
                }
            }
            catch (RetryLimitExceededException)
            {
                ModelState.AddModelError("", "Có lỗi xảy ra");
            }
            return RedirectToAction("Index");
        }
    }
}