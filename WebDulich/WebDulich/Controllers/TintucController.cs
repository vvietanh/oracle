﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebDulich.Models;
using System.IO;
using System.Data.Entity.Infrastructure;
using PagedList;
using PagedList.Mvc;

namespace WebDulich.Controllers
{
    public class TintucController : Controller
    {
        DULICHEntities1 data = new DULICHEntities1();

        public ActionResult Index(int ?page)
        {
            int pageNumber = (page ?? 1);
            int pageSize = 20;
            // return View(data.TINTUCs.ToList());
            return View(data.TINTUCs.ToList().OrderBy(n => n.ID).ToPagedList(pageNumber, pageSize));
        }

    }
}