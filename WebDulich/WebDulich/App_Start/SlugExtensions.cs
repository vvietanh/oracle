﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;


namespace WebDulich.App_Start
{
    public static class SlugExtensions
    {
        private static readonly Dictionary<string, char> ReplaceTable = new Dictionary<string, char>()
        {
            { "ăâàằầáắấảẳẩãẵẫạặậ", 'a' },
            { "êèềéếẻểẽễẹệ", 'e' },
            { "ìíỉĩị", 'i' },
            { "ôơòồờóốớỏổỏõỗỡọộợ", 'o' },
            { "ưùừúứủửũữụự", 'u' },
            { "ỳýỷỹỵ", 'y' }
        };
        public static string GenerateSlug(this string phrase)
        {
            string str = phrase.ToLower().ReplaceVietnameseAccent().RemoveAccent().ToLower();
            // invalid chars           
            str = Regex.Replace(str, @"[^a-z0-9\s-]", "");
            // convert multiple spaces into one space   
            str = Regex.Replace(str, @"\s+", " ").Trim();
            // cut and trim 
            str = str.Substring(0, str.Length <= 500 ? str.Length : 500).Trim();
            str = Regex.Replace(str, @"\s", "-"); // hyphens   
            return str;
        }

        public static string RemoveAccent(this string txt)
        {
            byte[] bytes = System.Text.Encoding.GetEncoding("Cyrillic").GetBytes(txt);
            return System.Text.Encoding.ASCII.GetString(bytes);
        }
        public static string ReplaceVietnameseAccent(this string txt)
        {
            bool ap;
            StringBuilder strb = new StringBuilder();
            for (int i = 0; i < txt.Length; i++)
            {
                ap = false;
                foreach (var d in ReplaceTable)
                    if (d.Key.Contains(txt[i]))
                    {
                        strb.Append(d.Value);
                        ap = true;
                        continue;
                    }
                if (!ap)
                    strb.Append(txt[i]);
            }
            return strb.ToString();
        }
       

    }
}